{-# LANGUAGE OverloadedStrings #-}

module Language.EFLINT.Check.Counterexample where

import Language.EFLINT.Check.Model

import Data.List (find)
import Data.Text (Text(..), unpack, splitOn, breakOn, replace, append)
import Text.HTML.TagSoup
import Text.Pretty.Simple

data Counterexample = Counterexample
  { cexPropertyId :: Int
  , cexScenario :: [Text]
  } deriving (Read, Show, Eq, Ord)

createCounterexample :: Text -> MSymbolTable -> Counterexample
createCounterexample xml st = Counterexample
                                { cexPropertyId = getPropertyId tags
                                , cexScenario = traceVarsToEFlint (getTraceVars tags) st
                                }
  where tags = parseTags xml


getPropertyId :: [Tag Text] -> Int
getPropertyId t = case find (~== TagOpen ("counter-example" :: Text) []) t of
  Just t' -> (read . unpack) (fromAttrib ("id" :: Text) t')
  Nothing -> error "Tag with `counter-example` not present"

getTraceVars :: [Tag Text] -> [Text]
getTraceVars t = map ((\ (TagText t') -> t') . (!! 1)) $ partitions (~== TagOpen ("value" :: Text) []) t

traceVarsToEFlint :: [Text] -> MSymbolTable -> [Text]
traceVarsToEFlint tvs st = map tv2eflint (filter (/= "none") tvs)
  where tv2eflint tv = case splitOn "$" tv of
          [ident] -> originalIdent ident st `append` "."
          [ident, params] -> originalIdent ident st `append` "("
                                                    `append` replace "_" ", " params
                                                    `append` ")."

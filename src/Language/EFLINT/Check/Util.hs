module Language.EFLINT.Check.Util where

-- Map function, but with an extra flag to indicate that we have reached the last item in
-- the list.
-- Example:
-- mapWithLast (\ x l -> if l then x * 2 else x) [1, 2, 3]
-- > [1, 2, 6]
mapWithLast :: (a -> Bool -> b) -> [a] -> [b]
mapWithLast _ [] = []
mapWithLast f [x] = [f x True]
mapWithLast f (x:xs) = f x False : mapWithLast f xs

-- Map function, but with an extra flag to indicate that we are dealing with the first
-- item in the list.
-- Example:
-- mapWithFirst (\ x l -> if l then x * 2 else x) [1, 2, 3]
-- > [2, 2, 3]
mapWithFirst :: (a -> Bool -> b) -> [a] -> [b]
mapWithFirst _ [] = []
mapWithFirst f (x:xs) = f x True : map (`f` False) xs

keys :: [(a, b)] -> [a]
keys = map fst

elems :: [(a, b)] -> [b]
elems = map snd

infixl 9 !

(!) :: Eq a => [(a, b)] -> a -> b
(!) m k = case lookup k m of
  Just v -> v
  Nothing -> error "Value not found"

{-# LANGUAGE OverloadedStrings, TupleSections, RecordWildCards #-}

{-
  Model.hs

  This module is responsible for building the model that will be used for checking eFLINT
  specifications.
 -}

module Language.EFLINT.Check.Model where

import Language.EFLINT.Interpreter (OutputWriter, Output(..))
import Language.EFLINT.Spec
import Language.EFLINT.State (State(..), Info(..), TransInfo(..), Error(..), RuntimeError(..), Assignment(..), emptyInput)
import Language.EFLINT.Eval

import Language.EFLINT.Check.Util (keys, elems, (!))

import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Data.Text (Text(..) , replace, pack)
import Control.Monad
import Control.Monad.Writer (tell)

import Data.Foldable (foldrM)

import Debug.Trace

type MSymbolTable = M.Map Text String -- new ID -> original ID
type MFields = [(DomId, Tagged)]     -- alias -> original tag
type MAssignments = M.Map Tagged MAssignment
type MDeclarations = M.Map DomId MDeclaration
type MProperties = M.Map DomId MProperty

data Model = Model
  { modelSymbolTable  :: MSymbolTable
  , modelPropositions :: MDeclarations -- Facts and duties
  , modelActions      :: MDeclarations -- Acts and events
  , modelInitialState :: MAssignments
  , modelProperties   :: MProperties
  } deriving (Ord, Eq, Show)

data MDeclaration = MDeclaration
  { declLabel      :: DomId             -- Name of the property.
  , declAttrs      :: MDeclarationAttrs -- Attributes specific to each type of property.
  } deriving (Ord, Eq, Show, Read)

data MDeclarationAttrs = FactDecl MFactDecl
                       | DutyDecl MDutyDecl
                       | TransitionDecl MTransDecl
                deriving (Ord, Eq, Show, Read)

data MFactType = AtomicString | AtomicInt | Bool | RecordType deriving (Ord, Eq, Show, Read)

data MFactDecl = MFactDecl
  { factInstances :: [MFactInstance]
  , factFieldNames :: [DomId]
  , factType :: MFactType
  } deriving (Ord, Eq, Show, Read)

data MDutyDecl = MDutyDecl
  { dutyInstances    :: [MDutyInstance]
  , dutyFieldNames   :: [DomId]
  } deriving (Ord, Eq, Show, Read)

data MFactInstance = MFactInstance
  { factInstanceFields :: MFields
  , factInstanceDerivation :: ITerm
  } deriving (Ord, Eq, Show, Read)

data MDutyInstance = MDutyInstance
  { dutyInstanceFields :: MFields
  , dutyInstanceDerivation :: ITerm
  , dutyInstanceViolation :: ITerm
  } deriving (Ord, Eq, Show, Read)

newtype MTransDecl = MTransDecl
  { transInstances :: [MTransInstance] } deriving (Ord, Eq, Show, Read)

data MTransInstance = MTransInstance
  { transInstanceFields   :: MFields
  , transInstancePrecon   :: ITerm
  , transInstancePostcons :: MAssignments
  } deriving (Ord, Eq, Show, Read)

data MProperty = MProperty
  { propertyName        :: DomId
  , propertyIsInvariant :: Bool
  , propertyInstances   :: Either ITerm [(MFields, ILTLTerm)]
  } deriving (Ord, Eq, Show, Read)

data MAssignment = MATrue
                 | MAFalse
                 | MASelf
                 deriving (Ord, Eq, Show, Read)

emptyModel = Model
  { modelSymbolTable  = M.empty
  , modelPropositions = M.empty
  , modelActions  = M.empty
  , modelInitialState = M.empty
  , modelProperties   = M.empty
  }

getModel :: Spec -> State -> OutputWriter (Maybe Model)
getModel spec state = case runSubs makeModel spec state emptyInput M.empty of
  Left err      -> tell [ErrorVal $ RuntimeError err] >> return Nothing
  Right [model] -> return $ Just model
  Right []      -> return $ Just emptyModel
  Right _       -> error "unknown error"

makeModel :: M_Subs Model
makeModel = do
  modelSymbolTable  <- mkSymbolTable
  modelPropositions <- mkProps
  modelInitialState <- mkInitialState (M.elems modelPropositions)
  modelActions      <- mkTransitions
  modelProperties   <- mkProperties
  return Model{..}

domain2factType :: Domain -> MFactType
domain2factType (Strings _) = AtomicString
domain2factType (Ints _) = AtomicInt
domain2factType (Products []) = Bool
domain2factType (Products _) = RecordType

getFactType :: MDeclaration -> MFactType
getFactType decl = case declAttrs decl of
                     FactDecl fDecl -> factType fDecl
                     _              -> error "operation not supported for this declaration type"

-- TODO nested Products?
getPropFields :: DomId -> Domain -> [DomId]
getPropFields domId (Products ps) = map (\ (Var d1 d2) -> d1 ++ d2) ps
getPropFields domId _ = [domId]

mkSymbolTable :: M_Subs MSymbolTable
mkSymbolTable = do
  idents <- M.keys . decls <$> get_spec
  return $ M.fromList $ map (\ i -> (sanitizeIdent i, i)) idents

sanitizeIdent :: String -> Text
sanitizeIdent ident =  replace "\'" "_p"
                       $ replace "-" "_"
                       $ replace " " "_"
                       $ replace "[" ""
                       $ replace "\"" ""
                       $ replace "]" "" (pack ident)

originalIdent :: Text -> MSymbolTable -> Text
originalIdent ident st = case M.lookup ident st of
  Just ident' -> pack ident'
  Nothing     -> error $ "not found: " ++ show ident

mkProperties :: M_Subs MProperties
mkProperties = do
  spec <- get_spec
  let inv_ids = S.toList $ invariants spec
  let ltl_ids = S.toList $ properties spec
  invars <- mapM mkInvariant inv_ids
  ltl_props <- mapM mkLTLProperty ltl_ids
  return $ M.fromList (zip inv_ids invars ++ zip ltl_ids ltl_props)

mkInvariant :: DomId -> M_Subs MProperty
mkInvariant domId = do
  term <- unfoldAggregators =<< getDerivation domId
  is <- instantiateTerm term []
  return MProperty {
    propertyName = domId
  , propertyIsInvariant = True
  , propertyInstances = Left is
  }

mkLTLProperty :: DomId -> M_Subs MProperty
mkLTLProperty domId = do
  t <- getLTLDerivation domId
  case t of
    LTLForEach vs t' -> do
      let ds = map (\ (Var d1 d2) -> d1 ++ d2) vs
      fis <- mapM ip vs
      let fs = map (zip ds) (sequence fis)
      is <- mapM (instantiateLTLTerm t') fs
      return MProperty {
        propertyName = domId
      , propertyIsInvariant = False
      , propertyInstances = Right (zip fs is)
      }
    t' -> do it <- instantiateLTLTerm t' []
             return MProperty {
                propertyName = domId
              , propertyIsInvariant = False
              , propertyInstances = Right [([], it)]
              }
 where ip v = do results $ every_valid_subs v

mkProps :: M_Subs MDeclarations
mkProps = do
  ids <- domIds
  modelPropositions <- mapM mkProp ids
  return $ M.fromList (zip ids modelPropositions)
  where domIds = do
          spec <- get_spec
          return $ M.keys $ M.filterWithKey (\ k _ -> isProp spec k) (decls spec)
        isProp spec p = p `notElem` ["actor", "int", "ref", "string"]
                          && not (triggerable spec p)
                          && not (is_invariant spec p)
                          && not (is_property spec p)

mkTransitions :: M_Subs MDeclarations
mkTransitions = do
  ids <- transIds
  modelPropositions <- mapM mkProp ids
  return $ M.fromList (zip ids modelPropositions)
  where transIds = do
          spec <- get_spec
          return $ M.keys $ M.filterWithKey (\ k _ -> triggerable spec k) (decls spec)

mkProp :: DomId -> M_Subs MDeclaration
mkProp domId = do
  attrs <- mkPropAttrs domId
  dv <- unfoldAggregators =<< getDerivation domId
  return MDeclaration { declLabel = domId
                      , declAttrs = attrs
                      }

mkPropAttrs :: DomId -> M_Subs MDeclarationAttrs
mkPropAttrs domId = do
  tspec <- get_type_spec domId
  case kind tspec of
    Fact fs  -> FactDecl <$> mkFactAttrs domId
    Duty ds  -> DutyDecl <$> mkDutyAttrs domId
    Act as   -> TransitionDecl <$> mkTransitionAttrs domId
    Event es -> TransitionDecl <$> mkTransitionAttrs domId

mkFactAttrs :: DomId -> M_Subs MFactDecl
mkFactAttrs domId = do
  (dom, _) <- get_dom domId
  fs <- mkFields domId
  fis <- mkFactInstances domId fs
  return MFactDecl { factInstances = fis
                   , factFieldNames = keys $ head fs
                   , factType = domain2factType dom
                   }

mkDutyAttrs :: DomId -> M_Subs MDutyDecl
mkDutyAttrs domId = do
  fs  <- mkFields domId
  fis <- mkDutyInstances domId fs
  vc  <- getDutyViolationCond domId
  return MDutyDecl { dutyInstances = fis
                   , dutyFieldNames = keys $ head fs
                   }

mkTransitionAttrs :: DomId -> M_Subs MTransDecl
mkTransitionAttrs domId = do
  fis <- mkFields domId
  tis <- mkTransInstances domId fis
  precon <- getCondition domId
  return MTransDecl { transInstances = tis }

-- Filter instances that cannot exist, according to their domain constraint.
validInstances :: DomId -> M_Subs [Elem]
validInstances domId = do
  (dom, _) <- get_dom domId
  elems <- results $ instantiate_domain domId dom
  filterM (\ e -> is_valid_instance (e, domId)) elems

mkFields :: DomId -> M_Subs [MFields]
mkFields domId = do
  (dom, _) <- get_dom domId
  is <- results $ case dom of
    AnyString -> err (ModelError $ "Domain for Fact " ++ domId ++ " is unbounded")
    AnyInt    -> err (ModelError $ "Domain for Fact " ++ domId ++ " is unbounded")
    _         -> instantiate_domain domId dom

  let pfs = getPropFields domId dom
  return $ map (mf pfs) is
  where mf pfs (Product ts) = zip pfs ts
        mf [pf] t = [(pf, (t, domId))]

mkFactInstances :: DomId -> [MFields] -> M_Subs [MFactInstance]
mkFactInstances domId = mapM mkFactInstance
  where mkFactInstance fi = do dv <- unfoldAggregators =<< getDerivation domId
                               idv <- instantiateTerm dv fi
                               return MFactInstance { factInstanceFields = fi
                                                    , factInstanceDerivation = idv
                                                    }

mkDutyInstances :: DomId -> [MFields] -> M_Subs [MDutyInstance]
mkDutyInstances domId = mapM mkDutyInstance
  where mkDutyInstance fi = do dv <- unfoldAggregators =<< getDerivation domId
                               idv <- instantiateTerm dv fi
                               vc <- unfoldAggregators =<< getDutyViolationCond domId
                               ivc <- instantiateTerm vc fi
                               return MDutyInstance { dutyInstanceFields = fi
                                                    , dutyInstanceDerivation = idv
                                                    , dutyInstanceViolation = ivc
                                                    }


mkTransInstances :: DomId -> [MFields] -> M_Subs [MTransInstance]
mkTransInstances domId = mapM mkTransInstance
  where mkTransInstance fi = do ti <- instantiate_trans (fields2elem fi RecordType, domId)
                                dv <- unfoldAggregators =<< getDerivation domId
                                cd <- unfoldAggregators =<< getCondition domId
                                dc <- unfoldAggregators =<< getDomConstraint domId

                                idv <- case dv of
                                        (BoolLit False) -> instantiateTerm (disjunctTerms [cd, dc]) fi
                                        _               -> instantiateTerm (disjunctTerms [dv, cd, dc]) fi
                                return MTransInstance { transInstanceFields   = fi
                                                      , transInstancePrecon = idv
                                                      , transInstancePostcons = M.map assignment2modelAssignment (trans_assignments ti)
                                                      }

getCondition :: DomId -> M_Subs Term
getCondition domId = do
  tspec <- get_type_spec domId
  return $ disjunctTerms $ conditions tspec

getDerivation :: DomId -> M_Subs Term
getDerivation domId = do
  tspec <- get_type_spec domId
  return $ concatenateDerivations $ derivation tspec

getDomConstraint :: DomId -> M_Subs Term
getDomConstraint domId = do
  tspec <- get_type_spec domId
  return $ domain_constraint tspec

getLTLDerivation :: DomId -> M_Subs LTLTerm
getLTLDerivation domId = do
  tspec <- get_type_spec domId
  return $ concatenateLTLDerivations $ derivation tspec

getDutyViolationCond :: DomId -> M_Subs Term
getDutyViolationCond domId = do
  tspec <- get_type_spec domId
  case kind tspec of
    Duty ds -> return $ conjunctTerms (violated_when ds)
    _       -> error "operation not supported"

atomicFactProps :: MDeclarations -> MDeclarations
atomicFactProps = M.filter (isAtomic . declAttrs)
  where isAtomic (FactDecl f) = case factType f of
                                  AtomicInt    -> True
                                  AtomicString -> True
                                  _            -> False
        isAtomic _            = False

recordFactProps :: MDeclarations -> MDeclarations
recordFactProps = M.filter (isRecord . declAttrs)
  where isRecord (FactDecl f) = case factType f of
                                  RecordType -> True
                                  _          -> False
        isRecord _            = False

boolFactProps :: MDeclarations -> MDeclarations
boolFactProps = M.filter (isBool . declAttrs)
  where isBool (FactDecl f) = case factType f of
                                  Bool -> True
                                  _    -> False
        isBool _            = False

dutyProps :: MDeclarations -> MDeclarations
dutyProps = M.filter (isDuty . declAttrs)
  where isDuty (DutyDecl _) = True
        isDuty _            = False

-- Get the initial assignments from the eFLINT state, and set all other assignments to False
mkInitialState :: [MDeclaration] -> M_Subs MAssignments
mkInitialState modelPropositions = do
  stateProps <- filterState <$> (contents <$> get_state)
  truthAssignments <- M.map (bool2modelAssignment . value) <$> stateProps
  let frame = M.fromList $ concatMap (map (, MAFalse) . prop2taggedInstances) modelPropositions
  return $ M.union truthAssignments frame
  where filterState state = do
          spec <- get_spec
          return $ M.filterWithKey (\ (_, domId) _ -> isProp spec domId) state
        isProp spec p = p `notElem` ["actor", "int", "ref", "string"]
                          && not (triggerable spec p)
                          && not (is_invariant spec p)
                          && not (is_property spec p)

concatenateDerivations :: [Derivation] -> Term
concatenateDerivations []                    = BoolLit False
concatenateDerivations [HoldsWhen t]         = t
concatenateDerivations ((HoldsWhen t):ds)    = Or t (concatenateDerivations ds)
concatenateDerivations _                     = error "not yet implemented"

concatenateLTLDerivations :: [Derivation] -> LTLTerm
concatenateLTLDerivations []                    = LTLEmpty
concatenateLTLDerivations [LTLHoldsWhen t]      = t
concatenateLTLDerivations ((LTLHoldsWhen t):ds) = LTLAnd t (concatenateLTLDerivations ds)
concatenateLTLDerivations _                     = error "not yet implemented"

disjunctTerms :: [Term] -> Term
disjunctTerms []     = BoolLit True
disjunctTerms [t]    = t
disjunctTerms (t:ts) = And t (disjunctTerms ts)

conjunctTerms :: [Term] -> Term
conjunctTerms []     = BoolLit False
conjunctTerms [t]    = t
conjunctTerms (t:ts) = Or t (conjunctTerms ts)

sumTerms :: [Term] -> Term
sumTerms []     = IntLit 0
sumTerms [t]    = t
sumTerms (t:ts) = Add t (sumTerms ts)

unfoldAggregators :: Term -> M_Subs Term
unfoldAggregators term = case term of
  Exists vs t -> do
    ts <- instantiateVars vs
    terms <- mapM (substituteRefs vs t) ts
    return $ conjunctTerms terms
  Forall vs t -> do
    ts <- instantiateVars vs
    terms <- mapM (substituteRefs vs t) ts
    return $ disjunctTerms terms
  Forall vs t -> do
    ts <- instantiateVars vs
    terms <- mapM (substituteRefs vs t) ts
    return $ sumTerms terms

  Not t       -> Not <$> unfoldAggregators t

  And t1 t2   -> liftM2 And (unfoldAggregators t1) (unfoldAggregators t2)
  Or t1 t2    -> liftM2 Or (unfoldAggregators t1) (unfoldAggregators t2)
  Eq t1 t2    -> liftM2 Eq (unfoldAggregators t1) (unfoldAggregators t2)
  Neq t1 t2   -> liftM2 Neq (unfoldAggregators t1) (unfoldAggregators t2)
  Leq t1 t2   -> liftM2 Leq (unfoldAggregators t1) (unfoldAggregators t2)
  Geq t1 t2   -> liftM2 Geq (unfoldAggregators t1) (unfoldAggregators t2)
  Le t1 t2    -> liftM2 Le (unfoldAggregators t1) (unfoldAggregators t2)
  Ge t1 t2    -> liftM2 Ge (unfoldAggregators t1) (unfoldAggregators t2)
  Sub t1 t2   -> liftM2 Sub (unfoldAggregators t1) (unfoldAggregators t2)
  Add t1 t2   -> liftM2 Add (unfoldAggregators t1) (unfoldAggregators t2)
  Mult t1 t2  -> liftM2 Mult (unfoldAggregators t1) (unfoldAggregators t2)
  Mod t1 t2   -> liftM2 Mod (unfoldAggregators t1) (unfoldAggregators t2)
  Div t1 t2   -> liftM2 Div (unfoldAggregators t1) (unfoldAggregators t2)

  t           -> return t

instantiateVars :: [Var] -> M_Subs [[Tagged]]
instantiateVars vs = do is <- mapM (results . every_valid_subs) vs
                        return $ sequence is

substituteRefs :: [Var] -> Term -> [Tagged] -> M_Subs Term
substituteRefs vs = foldrM (`substituteRef` vs)

substituteRef :: Tagged -> [Var] -> Term -> M_Subs Term
substituteRef tag vars term = case term of
    Not t -> Not <$> substituteRef tag vars t
    BoolLit b -> return $ BoolLit b
    IntLit i -> return $ IntLit i
    StringLit s -> return $ StringLit s

    And t1 t2 -> liftM2 And (substituteRef tag vars t1) (substituteRef tag vars t2)
    Or t1 t2 -> liftM2 Or (substituteRef tag vars t1) (substituteRef tag vars t2)
    Eq t1 t2 -> liftM2 Eq (substituteRef tag vars t1) (substituteRef tag vars t2)
    Neq t1 t2 -> liftM2 Neq (substituteRef tag vars t1) (substituteRef tag vars t2)
    Leq t1 t2 -> liftM2 Leq (substituteRef tag vars t1) (substituteRef tag vars t2)
    Geq t1 t2 -> liftM2 Geq (substituteRef tag vars t1) (substituteRef tag vars t2)
    Le t1 t2 -> liftM2 Le (substituteRef tag vars t1) (substituteRef tag vars t2)
    Ge t1 t2 -> liftM2 Ge (substituteRef tag vars t1) (substituteRef tag vars t2)
    Sub t1 t2 -> liftM2 Sub (substituteRef tag vars t1) (substituteRef tag vars t2)
    Add t1 t2 -> liftM2 Add (substituteRef tag vars t1) (substituteRef tag vars t2)
    Mult t1 t2 -> liftM2 Mult (substituteRef tag vars t1) (substituteRef tag vars t2)
    Mod t1 t2 -> liftM2 Mod (substituteRef tag vars t1) (substituteRef tag vars t2)
    Div t1 t2 -> liftM2 Div (substituteRef tag vars t1) (substituteRef tag vars t2)

    Exists vs t -> Exists vs <$> substituteRef tag vars t
    Forall vs t -> Forall vs <$> substituteRef tag vars t
    Count vs t -> Count vs <$> substituteRef tag vars t
    Sum vs t -> Sum vs <$> substituteRef tag vars t
    Max vs t -> Max vs <$> substituteRef tag vars t
    Min vs t -> Min vs <$> substituteRef tag vars t
    When t1 t2 -> liftM2 When (substituteRef tag vars t1) (substituteRef tag vars t2)
    Present t -> Present <$> substituteRef tag vars t
    Violated t -> Violated <$> substituteRef tag vars t
    Enabled t -> Enabled <$> substituteRef tag vars t
    Project t v -> flip Project v <$> substituteRef tag vars t

    Ref v -> sub tag v
    Tag t d -> flip Tag d <$> substituteRef tag vars t
    Untag t -> Untag <$> substituteRef tag vars t
    App d (Left ts) -> do
      subs <- mapM (substituteRef tag vars) ts
      return $ App d (Left subs)
    App d (Right ms) -> do
      subs <- mapM (\ (Rename v r) -> Rename v <$> substituteRef tag vars r) ms
      return $ App d (Right subs)
    CurrentTime -> return CurrentTime
  where sub tag@(e, d) v = do
          spec <- get_spec
          let vd = remove_decoration spec v
          if vd == d && v `elem` vars
            then case e of
                           String s -> return $ Tag (StringLit s) d
                           Int i -> return $ Tag (IntLit i) d
                           Product ps -> return $ App d (Right $ map tag2rename ps)
            else return $ Ref v
        tag2rename (String e', d') = Rename (Var d' "") (Tag (StringLit e') d')
        tag2rename (Int e', d') = Rename (Var d' "") (Tag (IntLit e') d')
        tag2rename _            = error "operation not supported for this type"

bool2modelAssignment :: Bool -> MAssignment
bool2modelAssignment True = MATrue
bool2modelAssignment False = MAFalse

prop2taggedInstances :: MDeclaration -> [Tagged]
prop2taggedInstances prop = case declAttrs prop of
  FactDecl attrs -> map (\ fs -> (fields2elem (factInstanceFields fs) (factType attrs), declLabel prop)) (factInstances attrs)
  DutyDecl attrs -> map (\ fs -> (fields2elem (dutyInstanceFields fs) RecordType, declLabel prop)) (dutyInstances attrs)
  _              -> error "operation not supported for this type"

fields2elem :: MFields -> MFactType -> Elem
fields2elem fs RecordType = Product $ elems fs
fields2elem _  Bool       = Product []
fields2elem fs _          = let [(e, _)] = elems fs in e

assignment2bool :: Assignment -> Bool
assignment2bool HoldsTrue  = True
assignment2bool HoldsFalse = False
assignment2bool Unknown    = False

assignment2modelAssignment :: Assignment -> MAssignment
assignment2modelAssignment HoldsTrue  = MATrue
assignment2modelAssignment HoldsFalse = MAFalse
assignment2modelAssignment Unknown    = MAFalse

data ITerm = IPresent ITerm
           | ITaken ITerm
           | IViolated ITerm
           | IEnabled ITerm
           | IFact Tagged
           | IBool Tagged
           | IVal ITerm

           | INot ITerm
           | IAnd ITerm ITerm
           | IOr ITerm ITerm

           | IBoolLit Bool
           | IIntLit Int
           | IStringLit String

           | IEq ITerm ITerm
           | INeq ITerm ITerm
           | ILe ITerm ITerm
           | IGe ITerm ITerm
           | ILeq ITerm ITerm
           | IGeq ITerm ITerm

           | ISub ITerm ITerm
           | IAdd ITerm ITerm
           | IMult ITerm ITerm
           | IMod ITerm ITerm
           | IDiv ITerm ITerm

           deriving (Show, Ord, Eq, Read)

isBooleanFact :: DomId -> M_Subs Bool
isBooleanFact d = do
  (dom, _) <- get_dom d
  case dom of
    Products [] -> return True
    _           -> return False


instantiateTerm :: Term -> MFields -> M_Subs ITerm
instantiateTerm term fs = case term of
  Present t     -> IPresent <$> instantiateTerm t fs
  Taken t       -> ITaken <$> instantiateTerm t fs
  Violated t    -> IViolated <$> instantiateTerm t fs
  Enabled t     -> IEnabled <$> instantiateTerm t fs

  Ref (Var d1 d2) -> do
    let d = d1 ++ d2
    bool <- isBooleanFact d
    if bool then return $ IBool (fs ! d)
            else return $ IFact (fs ! d)
  App d a       -> return $ IFact (instantiateApp d a fs)
  Tag t d       -> case t of
                     StringLit s -> return $ IFact (String s, d)
                     IntLit i    -> return $ IFact (Int i, d)
  Untag t       -> instantiateTerm t fs

  BoolLit b     -> return $ IBoolLit b
  StringLit s   -> return $ IStringLit s
  IntLit i      -> return $ IIntLit i

  Not t         -> INot <$> instantiateTerm t fs
  And t1 t2     -> liftM2 IAnd (instantiateTerm t1 fs) (instantiateTerm t2 fs)
  Or t1 t2      -> liftM2 IOr (instantiateTerm t1 fs) (instantiateTerm t2 fs)

  Eq t1 t2      -> liftM2 IEq (IVal <$> instantiateTerm t1 fs) (IVal <$> instantiateTerm t2 fs)
  Neq t1 t2     -> liftM2 INeq (IVal <$> instantiateTerm t1 fs) (IVal <$> instantiateTerm t2 fs)
  Le t1 t2      -> liftM2 ILe (IVal <$> instantiateTerm t1 fs) (IVal <$> instantiateTerm t2 fs)
  Ge t1 t2      -> liftM2 IGe (IVal <$> instantiateTerm t1 fs) (IVal <$> instantiateTerm t2 fs)
  Leq t1 t2     -> liftM2 ILeq (IVal <$> instantiateTerm t1 fs) (IVal <$> instantiateTerm t2 fs)
  Geq t1 t2     -> liftM2 IGeq (IVal <$> instantiateTerm t1 fs) (IVal <$> instantiateTerm t2 fs)

  Sub t1 t2     -> liftM2 ISub (IVal <$> instantiateTerm t1 fs) (IVal <$> instantiateTerm t2 fs)
  Add t1 t2     -> liftM2 IAdd (IVal <$> instantiateTerm t1 fs) (IVal <$> instantiateTerm t2 fs)
  Mult t1 t2    -> liftM2 IMult (IVal <$> instantiateTerm t1 fs) (IVal <$> instantiateTerm t2 fs)
  Mod t1 t2     -> liftM2 IMod (IVal <$> instantiateTerm t1 fs) (IVal <$> instantiateTerm t2 fs)
  Div t1 t2     -> liftM2 IDiv (IVal <$> instantiateTerm t1 fs) (IVal <$> instantiateTerm t2 fs)
  t             -> error $ "not yet implemented: " ++ show t

instantiateApp :: DomId -> Arguments -> MFields -> Tagged
instantiateApp d (Left ms) fs = (Product $ map ia ms, d)
  where ia (Ref (Var r1 r2)) = let r = r1 ++ r2 in case lookup r fs of
                              (Just t) -> t
                              Nothing  -> error $ "not found: " ++ r
instantiateApp d (Right ms) fs = (Product $ map ia ms, d)
  where ia (Rename (Var _ _) (Tag t d)) = case t of
                                           StringLit s -> (String s, d)
                                           IntLit i    -> (Int i, d)
        ia (Rename (Var r1 r2) term) = let r = r1 ++ r2 in case lookup r fs of
                                      (Just t) -> t
                                      Nothing  -> error $ "not found: " ++ r

data ILTLTerm = ILTLNot ILTLTerm
              | ILTLAnd ILTLTerm ILTLTerm
              | ILTLOr ILTLTerm ILTLTerm
              | ILTLImplies ILTLTerm ILTLTerm
              | ILTLEquiv ILTLTerm ILTLTerm
              | ILTLAlways ILTLTerm
              | ILTLEventually ILTLTerm
              | ILTLNext ILTLTerm
              | ILTLUntil ILTLTerm ILTLTerm
              | ILTLEnabled ITerm
              | ILTLViolated ITerm
              | ILTLHolds ITerm
              | ILTLTaken ITerm
              | ILTLEmpty
              -- | ILTLTagged Tagged
              deriving (Show, Ord, Eq, Read)

instantiateLTLTerm :: LTLTerm -> MFields -> M_Subs ILTLTerm
instantiateLTLTerm term fs = case term of
  LTLNot t         -> ILTLNot <$> instantiateLTLTerm t fs
  LTLAnd t1 t2     -> liftM2 ILTLAnd (instantiateLTLTerm t1 fs) (instantiateLTLTerm t2 fs)
  LTLOr t1 t2      -> liftM2 ILTLOr (instantiateLTLTerm t1 fs) (instantiateLTLTerm t2 fs)
  LTLImplies t1 t2 -> liftM2 ILTLImplies (instantiateLTLTerm t1 fs) (instantiateLTLTerm t2 fs)
  LTLEquiv t1 t2   -> liftM2 ILTLEquiv (instantiateLTLTerm t1 fs) (instantiateLTLTerm t2 fs)
  LTLAlways t      -> ILTLAlways <$> instantiateLTLTerm t fs
  LTLEventually t  -> ILTLEventually <$> instantiateLTLTerm t fs
  LTLNext t        -> ILTLNext <$> instantiateLTLTerm t fs
  LTLUntil t1 t2   -> liftM2 ILTLUntil (instantiateLTLTerm t1 fs) (instantiateLTLTerm t2 fs)
  LTLEnabled t     -> ILTLEnabled <$> instantiateTerm t fs
  LTLViolated t    -> ILTLViolated <$> instantiateTerm t fs
  LTLHolds t       -> ILTLHolds <$> instantiateTerm t fs
  LTLTaken t       -> ILTLTaken <$> instantiateTerm t fs
  LTLForEach vs t  -> instantiateLTLTerm t fs
  LTLEmpty         -> return ILTLEmpty

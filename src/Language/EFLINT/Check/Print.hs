module Language.EFLINT.Check.Print where

import Language.EFLINT.Spec
import Language.EFLINT.Print

import Language.EFLINT.Check.Model

ppITerm :: ITerm -> String
ppITerm t = case t of
  INot t -> app "!" [ppITerm t]
  IAnd t1 t2 -> app_infix "&&" (ppITerm t1) (ppITerm t2)
  IOr t1 t2 -> app_infix "||" (ppITerm t1) (ppITerm t2)
  IEq t1 t2 -> app_infix "==" (ppITerm t1) (ppITerm t2)
  INeq t1 t2 -> app_infix "!=" (ppITerm t1) (ppITerm t2)
  ILeq t1 t2 -> app_infix "<=" (ppITerm t1) (ppITerm t2)
  IGeq t1 t2 -> app_infix ">=" (ppITerm t1) (ppITerm t2)
  IGe t1 t2 -> app_infix ">" (ppITerm t1) (ppITerm t2)
  ILe t1 t2 -> app_infix "<" (ppITerm t1) (ppITerm t2)

  ISub t1 t2 -> app_infix "-" (ppITerm t1) (ppITerm t2)
  IAdd t1 t2 -> app_infix "+" (ppITerm t1) (ppITerm t2)
  IMult t1 t2 -> app_infix "*" (ppITerm t1) (ppITerm t2)
  IMod t1 t2 -> app_infix "%" (ppITerm t1) (ppITerm t2)
  IDiv t1 t2 -> app_infix "/" (ppITerm t1) (ppITerm t2)

  IBoolLit b -> show b
  IIntLit i -> show i
  IStringLit s -> show s

  IPresent t   -> ppITerm t
  ITaken t     -> ppITerm t
  IViolated t  -> app "Violated" [ppITerm t]
  IEnabled t   -> app "Enabled" [ppITerm t]
  IFact t      -> ppTagged t
  IVal t       -> ppITerm t

ppILTLTerm :: ILTLTerm -> String
ppILTLTerm term = case term of
  ILTLNot t         -> "Not " ++ ppILTLTerm t
  ILTLAnd t1 t2     -> ppILTLTerm t1 ++ " And " ++ ppILTLTerm t2
  ILTLOr t1 t2      -> ppILTLTerm t1 ++ " Or " ++ ppILTLTerm t2
  ILTLImplies t1 t2 -> "If " ++ ppILTLTerm t1 ++ " Then " ++ ppILTLTerm t2
  ILTLEquiv t1 t2   -> ppILTLTerm t1 ++ " Iff " ++ ppILTLTerm t2
  ILTLAlways t      -> "Always " ++ ppILTLTerm t
  ILTLEventually t  -> "Finally " ++ ppILTLTerm t
  ILTLNext t        -> "Next " ++ ppILTLTerm t
  ILTLUntil t1 t2   -> ppILTLTerm t1 ++ " Until " ++ ppILTLTerm t2
  ILTLEnabled t     -> "Enabled(" ++ ppITerm t ++ ")"

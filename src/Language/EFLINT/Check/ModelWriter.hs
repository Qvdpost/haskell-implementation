{-# LANGUAGE DeriveGeneric, OverloadedStrings, TemplateHaskell, RecordWildCards, TupleSections #-}

{-
  ModelWriter.hs

  This module is responsible for generating the nuXmv model from the internal Model
  (see Model.hs), using the Mustache templating system.
 -}

module Language.EFLINT.Check.ModelWriter where

import Language.EFLINT.Spec (DomId, Elem(..), Tagged)
import Language.EFLINT.Check.Model
import Language.EFLINT.Check.Util

import Data.Aeson hiding (String)
import Data.Text (Text, pack, intercalate, append)
import GHC.Generics
import Text.Mustache
import qualified Text.Mustache.Compile.TH as TH
import Data.Text.Lazy (toStrict)
import qualified Data.Map as M

import Control.Monad.Reader (Reader, asks, runReader)

type ModelReader = Reader Model

data ModelOutput = ModelOutput
  { atomicFacts  :: [AtomicFactOutput]
  , recordFacts  :: [RecordFactOutput]
  , boolFacts    :: [BoolFactOutput]
  , duties       :: [DutyOutput]
  , initialState :: [StateOutput]
  , transitions  :: [TransOutput]
  , properties   :: [PropertyOutput]
  } deriving (Eq, Show, Generic)

instance ToJSON ModelOutput

data AtomicFactOutput = AtomicFactOutput
  { afName      :: Text
  , afIsString  :: Bool
  , afInstances :: [FactInstanceOutput]
  , afLast      :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON AtomicFactOutput

data RecordFactOutput = RecordFactOutput
  { rfName       :: Text
  , rfFields     :: Text
  , rfInstances  :: [FactInstanceOutput]
  , rfLast       :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON RecordFactOutput

data BoolFactOutput = BoolFactOutput
  { bName      :: Text
  , bLast      :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON BoolFactOutput

data FactInstanceOutput = FactInstanceOutput
  { fiIdentifier :: Text
  , fiParams     :: [FieldParamOutput]
  , fiDerivation :: Text
  , fiLast       :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON FactInstanceOutput

data DutyOutput = DutyOutput
  { dName      :: Text
  , dFields    :: Text
  , dInstances :: [DutyInstanceOutput]
  , dLast      :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON DutyOutput

data DutyInstanceOutput = DutyInstanceOutput
  { diIdentifier   :: Text
  , diParams       :: [FieldParamOutput]
  , diDerivation   :: Text
  , diViolationCon :: Text
  , diLast         :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON DutyInstanceOutput

data FieldParamOutput = FieldParamOutput
  { fpValue    :: Text
  , fpIsString :: Bool
  , fpLast     :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON FieldParamOutput

data StateOutput = StateOutput
  { sIdentifier :: Text
  , sValue      :: Text
  , sIsBool     :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON StateOutput

data TransOutput = TransOutput
  { tName      :: Text
  , tInstances :: [TransInstanceOutput]
  , tLast      :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON TransOutput

data TransInstanceOutput = TransInstanceOutput
  { tiIdentifier :: Text
  , tiPrecon     :: Text
  , tiPostcons   :: [TransInstancePostconOutput]
  , tiLast       :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON TransInstanceOutput

data TransInstancePostconOutput = TransInstancePostconOutput
  { tpIdentifier       :: Text
  , tpAssignment       :: Text
  , tpAssignmentToSelf :: Bool
  , tpIsBool           :: Bool
  , tpFirst            :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON TransInstancePostconOutput

data PropertyOutput = PropertyOutput
  { pName        :: Text
  , pFormula     :: Text
  , pIsInvariant :: Bool
  } deriving (Eq, Show, Generic)

instance ToJSON PropertyOutput

newtype LTLOutput = LTLOutput { ltlFormula :: Text } deriving (Eq, Show, Generic)

instance ToJSON LTLOutput

modelOutput :: [ILTLTerm] -> ModelReader ModelOutput
modelOutput ltls = do
  atomicFacts  <- atomicFactOutput
  recordFacts  <- recordFactOutput
  boolFacts    <- boolFactOutput
  duties       <- dutyOutput
  initialState <- initialStateOutput
  transitions  <- transOutput
  properties   <- propertyOutput
  return ModelOutput
         { atomicFacts = atomicFacts
         , recordFacts = recordFacts
         , boolFacts = boolFacts
         , duties = duties
         , initialState = initialState
         , transitions = transitions
         , properties = properties
         }

atomicFactOutput :: ModelReader [AtomicFactOutput]
atomicFactOutput = do
  modelPropositions <- asks (atomicFactProps . modelPropositions)
  return $ mapWithLast af (M.elems modelPropositions)
  where af p l = AtomicFactOutput
                 { afName      = sanitizeIdent (declLabel p)
                 , afIsString  = getFactType p == AtomicString
                 , afInstances = factInstanceOutput p
                 , afLast      = l
                 }

recordFactOutput :: ModelReader [RecordFactOutput]
recordFactOutput = do
  modelPropositions <- asks (recordFactProps . modelPropositions)
  return $ mapWithLast rf (M.elems modelPropositions)
  where rf p l = RecordFactOutput
                 { rfName = sanitizeIdent (declLabel p)
                 , rfFields = propFieldsOutput (declAttrs p)
                 , rfInstances = factInstanceOutput p
                 , rfLast = l
                 }

boolFactOutput :: ModelReader [BoolFactOutput]
boolFactOutput = do
  modelPropositions <- asks (boolFactProps . modelPropositions)
  return $ mapWithLast bf (M.elems modelPropositions)
  where bf p l = BoolFactOutput
                 { bName = sanitizeIdent (declLabel p)
                 , bLast = l
                 }

dutyOutput :: ModelReader [DutyOutput]
dutyOutput = do
  modelPropositions <- asks (dutyProps . modelPropositions)
  return $ mapWithLast ds (M.elems modelPropositions)
  where ds p l = DutyOutput
                 { dName = sanitizeIdent (declLabel p)
                 , dFields = propFieldsOutput (declAttrs p)
                 , dInstances = dutyInstanceOutput (declLabel p) (declAttrs p)
                 , dLast = l
                 }

factInstanceOutput :: MDeclaration -> [FactInstanceOutput]
factInstanceOutput prop = mapWithLast afi (factInstances attrs)
  where (FactDecl attrs) = declAttrs prop
        afi ai l = let ident = instanceIdentifier (declLabel prop) (factInstanceFields ai) in
                   FactInstanceOutput
                    { fiIdentifier = ident
                    , fiParams = fieldParamOutput (fields2elem (factInstanceFields ai) (factType attrs))
                    , fiDerivation = formatITerm $ factInstanceDerivation ai
                    , fiLast = l
                    }

-- TODO refactor
propFieldsOutput :: MDeclarationAttrs -> Text
propFieldsOutput (FactDecl attrs) = intercalate ", " $ map sanitizeIdent (factFieldNames attrs)
propFieldsOutput (DutyDecl attrs) = intercalate ", " $ map sanitizeIdent (dutyFieldNames attrs)
propFieldsOutput (_) = error "operation not supported"

-- TODO refactor to MDeclaration -> [DutyInstanceOutput]
dutyInstanceOutput :: DomId -> MDeclarationAttrs -> [DutyInstanceOutput]
dutyInstanceOutput domId (DutyDecl d) = mapWithLast dio dis
  where dis = dutyInstances d
        dio di l = let ident = instanceIdentifier domId (dutyInstanceFields di) in
                   DutyInstanceOutput
                    { diIdentifier = ident
                    , diParams = fieldParamOutput (fields2elem (dutyInstanceFields di) RecordType)
                    , diDerivation = formatITerm $ dutyInstanceDerivation di
                    , diViolationCon = formatITerm $ dutyInstanceViolation di
                    , diLast = l
                    }
dutyInstanceOutput domId _ = error "operation not supported"

transOutput :: ModelReader [TransOutput]
transOutput = do
  ts <- asks modelActions
  sequence $ mapWithLast as (M.elems ts)
  where as t l = do ais <- transInstanceOutput t -- (transLabel t) (transInstances t)
                    return TransOutput
                           { tName = sanitizeIdent (declLabel t)
                           , tInstances = ais
                           , tLast = l
                           }

transInstanceOutput :: MDeclaration -> ModelReader [TransInstanceOutput]
transInstanceOutput prop = sequence $ mapWithLast ai tis
  where (TransitionDecl attrs) = declAttrs prop
        tis = transInstances attrs
        ai ti l = do let ident = instanceIdentifier (declLabel prop) (transInstanceFields ti)
                     postcons <- transInstancePostconOutput (transInstancePostcons ti)
                     return TransInstanceOutput
                            { tiIdentifier = ident
                            , tiPrecon = formatITerm (transInstancePrecon ti)
                            , tiPostcons = postcons
                            , tiLast = l
                            }

transInstancePostconOutput :: MAssignments -> ModelReader [TransInstancePostconOutput]
transInstancePostconOutput ps = do
  modelPropositions <- asks modelPropositions
  let frame = M.fromList $ concatMap (map (, MASelf) . prop2taggedInstances) modelPropositions
  let postcons = M.union ps frame
  return $ mapWithFirst ap (M.assocs postcons)
  where ap (t, a) f = TransInstancePostconOutput
                      { tpIdentifier       = tagged2identifier t
                      , tpAssignment       = assignment2text t a
                      , tpAssignmentToSelf = a == MASelf
                      , tpIsBool           = isBool t
                      , tpFirst            = f
                      }
        isBool (Product [], _) = True
        isBool _ = False

propertyOutput :: ModelReader [PropertyOutput]
propertyOutput = do
  modelProps <- asks modelProperties
  return $ concatMap (\ p -> either (invarOutput (propertyName p))
                              (ltlOutput (propertyName p))
                              (propertyInstances p)
               ) (M.elems modelProps)


invarOutput :: DomId -> ITerm -> [PropertyOutput]
invarOutput d t = [PropertyOutput { pName = sanitizeIdent d, pFormula = formatITerm t, pIsInvariant = True}]

ltlOutput :: DomId -> [(MFields, ILTLTerm)] -> [PropertyOutput]
ltlOutput d ts = map (\(fs, t) -> PropertyOutput { pName = instanceIdentifier d fs, pFormula = formatILTLTerm t, pIsInvariant = False}) ts

instanceId ::  DomId -> Elem -> Text
instanceId d e = sanitizeIdent d `append` delim (instanceElemOutput e)
  where delim "" = ""
        delim es = "$" `append` es

instanceIdentifier :: DomId -> MFields -> Text
instanceIdentifier d as = sanitizeIdent d `append` identifierElems (map (instanceElemOutput . fst) (elems as))
  where identifierElems [] = ""
        identifierElems es = "$" `append` intercalate "_" es

instanceElemOutput :: Elem -> Text
instanceElemOutput (String el)   = sanitizeIdent el
instanceElemOutput (Int el)      = pack $ show el
instanceElemOutput (Product els) = intercalate "_" $ map (instanceElemOutput . fst) els

elemIsString :: Elem -> Bool
elemIsString (String el) = True
elemIsString _           = False

fieldParamOutput :: Elem -> [FieldParamOutput]
fieldParamOutput (Product ts) = fieldParamOutput' ts
fieldParamOutput e            = [FieldParamOutput { fpValue = instanceElemOutput e, fpIsString = elemIsString e, fpLast = True }]

fieldParamOutput' :: [Tagged] -> [FieldParamOutput]
fieldParamOutput' [(e, _)]    = [FieldParamOutput { fpValue = instanceElemOutput e, fpIsString = elemIsString e, fpLast = True }]
fieldParamOutput' ((e, _):ts) =  FieldParamOutput { fpValue = instanceElemOutput e, fpIsString = elemIsString e, fpLast = False } : fieldParamOutput' ts
fieldParamOutput' []     = []

initialStateOutput :: ModelReader [StateOutput]
initialStateOutput = do
  iState <- asks modelInitialState
  return $ M.elems $ M.mapWithKey is iState
  where is t v = StateOutput
                   { sIdentifier = tagged2identifier t
                   , sIsBool = isBool t
                   , sValue = assignment2text t v
                   }
        isBool (Product [], _) = True
        isBool _ = False

formatITerm :: ITerm -> Text
formatITerm term = case term of
  IPresent t  -> formatITerm t `append` ".holds"
  ITaken t    -> "last_trans = " `append` formatITerm t
  IViolated t -> formatITerm t `append` ".violated"
  IEnabled t  -> formatITerm t `append` ".enabled"

  IFact t     -> tagged2identifier t
  IVal t      -> formatITerm t `append` ".val"

  IAnd e1 e2  -> "(" `append` formatITerm e1 `append` ") & (" `append` formatITerm e2 `append` ")"
  IOr e1 e2   -> "(" `append` formatITerm e1 `append` ") | (" `append` formatITerm e2 `append` ")"
  INot e      -> "!(" `append` formatITerm e `append` ")"

  IEq e1 e2   -> "(" `append` formatITerm e1 `append` ") = (" `append` formatITerm e2 `append` ")"
  INeq e1 e2  -> "(" `append` formatITerm e1 `append` ") != (" `append` formatITerm e2 `append` ")"
  ILe e1 e2   -> "(" `append` formatITerm e1 `append` ") < (" `append` formatITerm e2 `append` ")"
  IGe e1 e2   -> "(" `append` formatITerm e1 `append` ") > (" `append` formatITerm e2 `append` ")"
  ILeq e1 e2  -> "(" `append` formatITerm e1 `append` ") <= (" `append` formatITerm e2 `append` ")"
  IGeq e1 e2  -> "(" `append` formatITerm e1 `append` ") >= (" `append` formatITerm e2 `append` ")"

  ISub e1 e2  -> "(" `append` formatITerm e1 `append` ") - (" `append` formatITerm e2 `append` ")"
  IAdd e1 e2  -> "(" `append` formatITerm e1 `append` ") + (" `append` formatITerm e2 `append` ")"
  IMult e1 e2 -> "(" `append` formatITerm e1 `append` ") * (" `append` formatITerm e2 `append` ")"
  IMod e1 e2  -> "(" `append` formatITerm e1 `append` ") mod (" `append` formatITerm e2 `append` ")"
  IDiv e1 e2  -> "(" `append` formatITerm e1 `append` ") / (" `append` formatITerm e2 `append` ")"

  IBoolLit b   -> if b then "TRUE" else "FALSE"
  IStringLit s -> pack s

formatILTLTerm :: ILTLTerm -> Text
formatILTLTerm term = case term of
  ILTLNot t         -> "!(" `append` formatILTLTerm t `append` ")"
  ILTLAnd t1 t2     -> formatILTLTerm t1 `append` " & " `append` formatILTLTerm t2
  ILTLOr t1 t2      -> formatILTLTerm t1 `append` " | " `append` formatILTLTerm t2
  ILTLImplies t1 t2 -> formatILTLTerm t1 `append` " -> " `append` formatILTLTerm t2
  ILTLEquiv t1 t2   -> formatILTLTerm t1 `append` " <-> " `append` formatILTLTerm t2
  ILTLAlways t      -> "G (" `append` formatILTLTerm t `append` ")"
  ILTLEventually t  -> "F (" `append` formatILTLTerm t `append` ")"
  ILTLNext t        -> "X (" `append` formatILTLTerm t `append` ")"
  ILTLUntil t1 t2   -> "(" `append` formatILTLTerm t1 `append` " U " `append` formatILTLTerm t2 `append` ") | G " `append` formatILTLTerm t1
  ILTLEnabled t     -> formatITerm t `append` ".enabled"
  ILTLViolated t    -> formatITerm t `append` ".violated"
  ILTLHolds t       -> formatITerm t `append` ".holds"
  ILTLTaken t       -> "last_trans = " `append` formatITerm t

tagged2identifier :: Tagged -> Text
tagged2identifier = uncurry (flip instanceId)

assignment2text :: Tagged -> MAssignment  -> Text
assignment2text t MATrue  = "TRUE"
assignment2text t MAFalse = "FALSE"
assignment2text t MASelf  = tagged2identifier t

writeModel :: Model -> [ILTLTerm] -> IO Text
writeModel model ltlProps = do
  -- TODO remove absolute paths
  let template = $(TH.compileMustacheFile "/Users/florine/uva/eflint/haskell-implementation/check_assets/template.mustache") -- Will compile template at compile time
  -- template <- compileMustacheFile "/Users/florine/uva/eflint/haskell-implementation/check_assets/template.mustache" -- For debugging, compiles template at runtime
  let output = runReader (modelOutput ltlProps) model
  return $ toStrict $ renderMustache template (toJSON output)


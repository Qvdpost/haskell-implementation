{-# LANGUAGE OverloadedStrings, LambdaCase, TupleSections #-}

module Main where

import Language.EFLINT.Check.Model
import Language.EFLINT.Check.ModelWriter
import Language.EFLINT.Check.Counterexample

import Language.EFLINT.Interpreter (Config(..), Output(..), Program(..),
                                    initialConfig, collapse_programs, convert_programs, getOutput)
import Language.EFLINT.Explorer (defInterpreter)
import Language.EFLINT.Parse (parse_component, syn_phrases)
import Language.EFLINT.Spec
import Language.EFLINT.State (State(..), Error(..), emptyInput, print_error)

import Text.Pretty.Simple
import Text.Printf

import qualified Data.Text.IO as TIO
import qualified Data.Text as T
import Data.List
import qualified Data.Map as M

import System.Directory
import System.FilePath
import System.Process
import System.IO

import Control.Monad.Except

import Options.Applicative

-- TODO Add options to write/silence auxillary output files
data Options = Options
  { specPath    :: FilePath
  , propName    :: DomId
  , bound       :: Int
  , printModel  :: Bool
  , dontCheck   :: Bool
  , debug       :: Bool
  }

options :: Parser Options
options = Options
        <$> strArgument
            ( metavar "<FILENAME>.eflint"
           <> help "The eFLINT specification (and optionally current knowledge base) to check"
            )
        <*> strOption
            ( long "prop"
           <> short 'p'
           <> value ""
           <> help "The invariant or LTL property that needs to be checked"
            )
        <*> option auto
            ( long "max-steps"
           <> short 's'
           <> help"The maximum number of steps to check for each scenario"
           <> showDefault
           <> value 10
           <> metavar "INT"
            )
        <*> switch
            ( long "print-model"
           <> short 'P'
           <> help "Print the model to stdout"
            )
        <*> switch
            ( long "dont-check"
           <> short 'x'
           <> help "Do not invoke the model checker, but only build the model"
            )
        <*> switch
            ( long "debug"
           <> short 'd'
           <> help "Print debug statements"
            )

type ExceptOutput = ExceptT [Output] IO

main :: IO ()
main = do
  po <- execParser opts
  res <- runExceptT (check po)
  case res of
    Left output -> printErrors output
    Right _ -> return ()

  where opts = info (options <**> helper)
                  ( fullDesc
                 <> progDesc "Check if the given eFLINT satisfies the given properties"
                 <> header "eFLINT-check -- A model checker for norm specifications written in eFLINT"
                  )

check :: Options -> ExceptOutput ()
check opts = do
  (spec, state) <- mkConfig (specPath opts)

  -- liftIO $ pp $ show state -- FIXME remove

  when (debug opts) $ do
    liftIO $ pp $ show spec

  model <- mkModel spec state

  when (debug opts) $ do
    liftIO $ pp $ show model

  modelOutput <- liftIO $ writeModel model []

  currDir <- liftIO getCurrentDirectory
  let outputDir = currDir </> ".eflint-check"
  liftIO $ createDirectoryIfMissing False outputDir
  let basePath = outputDir </> takeBaseName (specPath opts)

  liftIO $ withFile (basePath <.> "smv") WriteMode $ \ h -> TIO.hPutStrLn h modelOutput

  when (printModel opts) $ liftIO $ TIO.putStrLn modelOutput

  -- We need to write the nuXmv output to a file and store it somewhere
  liftIO $ createConfig (propName opts) (bound opts) (modelProperties model) basePath

  unless (dontCheck opts) $ liftIO $ withFile (basePath <.> "out") WriteMode $ \ h -> do
    p <- createProcess (proc "nuXmv" ["-source", basePath <.> "scr", basePath <.> "smv"]) { std_out = UseHandle h, std_err = CreatePipe }
    case p of
      (_, _, Just hErr, ph) -> do err <- hGetContents hErr
                                  if err == "There are no traces currently available.\n"
                                    then putStrLn "No counterexamples found!"
                                  else if null err
                                    then mkCounterExample (basePath <.> "xml") (M.elems $ modelProperties model) (modelSymbolTable model)
                                  else putStrLn err
      _                     -> error "nuXmv error"
    liftIO $ cleanupProcess p

nuxmvBaseConfig = "set on_failure_script_quits\n\
              \set traces_regexp last_trans\n\
              \set default_trace_plugin 6\n\
              \go_msat\n\
              \build_boolean_model\n\
              \%s\n\
              \show_traces -o %s\n\
              \quit"

createConfig :: DomId -> Int -> (M.Map DomId MProperty) -> FilePath -> IO ()
createConfig domId bound props basePath = do
  let bnd = show bound
  let checkCommands = case M.lookup domId props of
        (Just p) -> case propertyInstances p of
          Left _   -> ["check_invar_ic3 -P " ++ domId ++ " -k " ++ bnd]
          Right ts -> map (\t -> "check_ltlspec_ic3 -P " ++ T.unpack (instanceIdentifier domId (fst t)) ++ " -k " ++ bnd) ts
        Nothing -> ["check_invar_ic3 -k " ++ bnd, "check_ltlspec_ic3 -k " ++ bnd]

  withFile (basePath <.> "scr") WriteMode $ \ h ->
              hPrintf h nuxmvBaseConfig (intercalate "\n" checkCommands) (basePath <.> "xml")


mkCounterExample :: FilePath -> [MProperty] -> MSymbolTable -> IO ()
mkCounterExample fp props st = do
  xml <- TIO.readFile fp
  let counterexamples = createCounterexample xml st
  let iProps = itemizeProperties props
  let violatingProp = iProps !! (cexPropertyId counterexamples - 1)
  putStrLn $ "Counterexample found! Property or invariant \'" ++ fst violatingProp ++ "\' was violated."
  putStrLn "The following scenario violates the above property or invariant:"
  TIO.putStrLn $ "\n  " `T.append` T.intercalate "\n  " (cexScenario counterexamples)
  currDir <- getCurrentDirectory
  let cePath = makeRelative currDir (fp -<.> "counterexample.eflint")
  TIO.writeFile cePath (T.unlines $ cexScenario counterexamples)
  putStrLn $ "\nCounterexample written to " ++ cePath

itemizeProperties :: [MProperty] -> [(DomId, (Either ITerm ILTLTerm))]
itemizeProperties ps = concatMap (\ p -> either (iInv $ propertyName p) (iLTL $ propertyName p) (propertyInstances p)) ps
  where iInv d it = [(d, Left it)]
        iLTL d its = map (\ (_, it) -> (d, Right it)) its

printErrors :: [Output] -> IO ()
printErrors output = putStrLn $ intercalate "" $ map print_error (errors output)

errors :: [Output] -> [Error]
errors [] = []
errors ((ErrorVal e):es) = e : errors es
errors (_:es) = errors es

mkConfig :: FilePath -> ExceptOutput (Spec, State)
mkConfig fp = do
  spec <- liftIO $ readFile fp
  ps <- parseSpecPhrases spec
  case interpretSpec ps of
    (Just cfg, _)     -> return (cfg_spec cfg, cfg_state cfg)
    (Nothing, output) -> throwError output

mkModel :: Spec -> State -> ExceptOutput Model
mkModel spec state = case getOutput $ getModel spec state of
                      (Just model, _)   -> return model
                      (Nothing, output) -> throwError output

interpretSpec :: [Phrase] -> (Maybe Config, [Output])
interpretSpec p = defInterpreter (emptyInput, getProgram p) (initialConfig Nothing)

parseSpecPhrases :: String -> ExceptOutput [Phrase]
parseSpecPhrases spec = case parse_component syn_phrases spec of
  Left err -> throwError [ErrorVal $ CompilationError err]
  Right p -> return $ specPhrases p

getProgram :: [Phrase] -> Program
getProgram = collapse_programs . convert_programs

specPhrases :: [Phrase] -> [Phrase]
specPhrases = filter isSpecPhrase
  where isSpecPhrase (PLTLQuery _ _) = False
        isSpecPhrase _               = True

pp :: String -> IO ()
pp = pPrintStringOpt CheckColorTty defaultOutputOptionsDarkBg {outputOptionsCompact = True}

pErrors :: [Error] -> IO ()
pErrors [] = return ()
pErrors errs = mapM_ op errs
  where op (CompilationError err) = putStr err
        op err                    = putStrLn (print_error err)
